#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 16 11:50:59 2017

@author: priyanka verma
"""
import numpy as np
from sklearn import svm

from fancyimpute import KNN
from sklearn.decomposition import PCA

from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier

import metrics

# Impute missing data using knn algorithm
def impute_knn(k,features):
    return KNN(k=3).complete(features)

# Reduce dimensions using PCA (principle component analysis)
def pca(n,features):
    pca = PCA(n_components = n)
    features_dim_n = pca.fit_transform(features)
    return features_dim_n

# Split classification data, 50% each in train and test sets
def test_train_split(features,labels):
    ad_samples = features[0:459,:]
    nonad_samples = features[459:,:]

    ad_labels = labels[:459]
    nonad_labels = labels[459:]

    X_train = ad_samples[0:len(ad_samples)/2]
    X_train = np.vstack((X_train, nonad_samples[0:len(nonad_samples)/2]))

    Y_train = np.concatenate((ad_labels[:len(ad_labels)/2],
                                        nonad_labels[:len(nonad_labels)/2]))

    X_test = ad_samples[len(ad_samples)/2:]
    X_test = np.vstack((X_test, nonad_samples[len(nonad_samples)/2:]))

    Y_test = np.concatenate((ad_labels[len(ad_labels)/2:],nonad_labels[len(nonad_labels)/2:]))

    return X_train, X_test, Y_train, Y_test

# classification using SVM
def svm_classifier(c,gamma,X_train,X_test,Y_train,Y_test):
    clf = svm.SVC(gamma=gamma,C=c, probability= True)
    clf.fit(X_train,Y_train)
    y_pred = clf.predict(X_test)
    metrics.compute_confusion_Matrix(Y_test, y_pred,['ad','nonad'],"svm.png")

    return clf.score(X_test,Y_test)

# classification using Naive Bayes
def naive_bayes_classifier(X_train,X_test,Y_train,Y_test):
    ## naive bayes classification
    gnb = GaussianNB()
    y_pred = gnb.fit(X_train, Y_train).predict(X_test)    
    metrics.compute_confusion_Matrix(Y_test, y_pred,['ad','nonad'],"naive.png")
    
    return gnb.score(X_test,Y_test)
    
# classification using MLPC (multi-layer perceptron - Neural network)
def mlpc_classifier(X_train,X_test,Y_train,Y_test):
    clf = MLPClassifier(solver='sgd', alpha=1e-5, hidden_layer_sizes=(70, 6),
                        random_state=1,learning_rate='adaptive')
    
    clf.fit(X_train,Y_train)
    y_pred = clf.predict(X_test)
    metrics.compute_confusion_Matrix(Y_test, y_pred,['ad','nonad'],"mlpc.png")

    return clf.score(X_test,Y_test)
