# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import sys,os

dir_path = '/Users/tushar/Credit_Suisse/Ad_classification/'

data = np.loadtxt(dir_path + 'ad.data',dtype='string',delimiter=',')
data = np.char.strip(data)
np.place(data,data=="?","NaN")
np.place(data,data=="ad.","1")
np.place(data,data=="nonad.","-1")

data = data.astype(float)


labels = data[:,-1]
features = data[:,:-1]

np.save(dir_path + "features_list.npy",features)
np.save(dir_path + "labels_list.npy",labels)