#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import argparse
import numpy as np

import classification

def loadData(file_path):
    data = np.loadtxt(file_path,dtype='string',delimiter=',')
    
    # Strip off leading/trainling spaces
    data = np.char.strip(data)
    
    # Replace missing entries(?) with NaN
    np.place(data,data == "?","NaN")
    
    # Replace ad with 1, to mark it as positive class 
    # And replace nonad with -1, to mark it as negative class
    np.place(data,data=="nonad.","-1")
    np.place(data,data=="ad.","1")
    
    # Change datatype to enable calculations
    #data = data.astype(float)

    #Separate features and labels from raw data
    labels = data[:,-1]
    features = data[:,:-1]
    
    return labels,features

def main(args):
    labels,features = loadData(args.datafile)    
    features = classification.impute_knn(3,features)
    features = classification.pca(10,features)
    
    X_train, X_test, Y_train, Y_test = \
                    classification.test_train_split(features,labels)
    
    svm_accuracy = classification.svm_classifier(100,0.001,X_train,X_test,
                                                 Y_train,Y_test)
    print "svm_accuracy = ", svm_accuracy
    print "******************************************"
    
    naive_accuracy = classification.naive_bayes_classifier(X_train,X_test,
                                                 Y_train,Y_test)
    print "naive_accuracy = ", naive_accuracy
    print "******************************************"
    
    mlpc_accuracy = classification.mlpc_classifier(X_train,X_test,Y_train,
                                                   Y_test)
    print "mlpc_accuracy = ", mlpc_accuracy
    print "******************************************"    
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--datafile',help="Full path of file : ad.data");
    args = parser.parse_args()
    
    main(args);